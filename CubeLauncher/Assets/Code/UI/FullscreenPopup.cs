using System;
using Code.Base;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace de.jacklog.cubeLauncher
{
    public class FullscreenPopup : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _bg;
        [SerializeField] private CanvasGroup _popup;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private CanvasGroup _btn;

        [SerializeField] private GameEvent _event;

        private void Start()
        {
            _event.Callback += OnEventTriggerd;
        }

        private void OnDisable()
        {
            _event.Callback -= OnEventTriggerd;
        }

        private void OnEventTriggerd()
        {
            PlayBackground();
            PlayPopup();
            PlayText();
            PlayButton();
        }

        private void PlayButton()
        {
            _btn.DOKill();
            _btn.DOFade(1, 0.5f).SetDelay(2f);
        }

        private void PlayText()
        {
            _text.DOKill();
            _text.transform.DOScale(1, 0.75f).SetEase(Ease.OutExpo).SetDelay(1f);
            _text.DOFade(1, 0.5f).SetDelay(1f);
        }

        private void PlayPopup()
        {
            _popup.DOKill();
            _popup.transform.DOLocalMoveY(0, 1f).SetEase(Ease.OutBounce).SetDelay(0.5f);;
            _popup.DOFade(1, 0.5f).SetDelay(0.5f);
        }

        private void PlayBackground()
        {
            _bg.DOKill();
            _bg.DOFade(1, 0.5f);
            _bg.interactable = true;
            _bg.blocksRaycasts = true;
        }
    }
}