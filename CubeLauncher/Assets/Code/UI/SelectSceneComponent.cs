using UnityEngine;
using UnityEngine.SceneManagement;

namespace de.jacklog.cubeLauncher
{
    public class SelectSceneComponent : MonoBehaviour
    {
        [SerializeField] private string _sceneName;

        public void LoadScene()
        {
            SceneManager.LoadScene(_sceneName);
        }
    }
}