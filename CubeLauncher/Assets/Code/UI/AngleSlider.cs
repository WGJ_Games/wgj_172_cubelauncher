using System;
using UnityEngine;
using UnityEngine.UI;

namespace de.jacklog.cubeLauncher
{
    public class AngleSlider : MonoBehaviour
    {
        [SerializeField] private Transform _arrow;
        [SerializeField] private Slider _slider;
        
        [SerializeField] private float _minAngle;
        [SerializeField] private float _maxAngle;

        private void Start()
        {
            _slider.onValueChanged.AddListener(AdjustAngle);
        }

        private void AdjustAngle(float sliderVal)
        {
            var angle = Mathf.Lerp(_minAngle, _maxAngle, sliderVal);
            var rotation = _arrow.rotation.eulerAngles;
            rotation.z = angle;
            _arrow.rotation = Quaternion.Euler(rotation);
        }
    }
}