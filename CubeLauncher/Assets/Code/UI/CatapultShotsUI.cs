using System;
using Code.Base;
using TMPro;
using UnityEngine;

namespace de.jacklog.cubeLauncher
{
    public class CatapultShotsUI : MonoBehaviour
    {
        [SerializeField] private IntProperty _shots;
        [SerializeField] private TextMeshProUGUI _label;

        private void Start()
        {
            _shots.OnChange += UpdateText;
            UpdateText(_shots.Value);
        }

        private void UpdateText(int shots)
        {
            _label.text = shots.ToString();
        }
    }
}