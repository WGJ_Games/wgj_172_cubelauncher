using UnityEngine;
using UnityEngine.SceneManagement;

namespace de.jacklog.cubeLauncher
{
    public class RetryComponent : MonoBehaviour
    {
        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}