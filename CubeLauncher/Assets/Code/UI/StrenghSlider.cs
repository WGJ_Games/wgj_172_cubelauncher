using de.jacklog.cubeLauncher.Code.Game;
using UnityEngine;
using UnityEngine.UI;

namespace de.jacklog.cubeLauncher
{
    public class StrenghSlider : MonoBehaviour
    {
        [SerializeField] private Transform _arrow;
        [SerializeField] private Slider _slider;
        
        [SerializeField] private float _minScale;
        [SerializeField] private float _maxMaxScale;

        [SerializeField] private CatapultData _catapultData;

        private void Start()
        {
            _slider.onValueChanged.AddListener(AdjustAngle);
        }

        private void AdjustAngle(float sliderVal)
        {
            _catapultData.Strength = sliderVal;
            var sliderScale = Mathf.Lerp(_minScale, _maxMaxScale, sliderVal);
            var scale = _arrow.localScale;
            scale.x = sliderScale;
            _arrow.localScale = scale;
        }
    }
}