using System;
using de.jacklog.cubeLauncher.Code.Game;
using UnityEngine;
using UnityEngine.UI;

namespace de.jacklog.cubeLauncher
{
    public class LaunchButton : MonoBehaviour
    {
        [SerializeField] private Transform _arrow;
        [SerializeField] private float _minScale;
        [SerializeField] private float _maxMaxScale;
        [SerializeField] private float _maxHoldTime;
        
        [SerializeField] private CatapultData _data;
        private float _holdTime;
        private bool _hold;

        private void Start()
        {
            _holdTime = 0;
            _hold = false;
        }

        public void BuildUpStrength()
        {
            _hold = true;
        }

        private void Update()
        {
            if (_hold)
            {
                _holdTime += Time.deltaTime;
                _data.Strength = Mathf.Clamp01(_holdTime / _maxHoldTime);
                var sliderScale = Mathf.Lerp(_minScale, _maxMaxScale, _data.Strength);
                var scale = _arrow.localScale;
                scale.x = sliderScale;
                _arrow.localScale = scale;
            }
        }

        public void Launch()
        {
            _data.OnLaunch.Invoke();
            _hold = false;
            _holdTime = 0;
        }
    }
}