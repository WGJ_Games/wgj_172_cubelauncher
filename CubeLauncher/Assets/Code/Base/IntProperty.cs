using System;
using UnityEngine;

namespace Code.Base
{
    [CreateAssetMenu(fileName = "Property", menuName = "Properties/Int", order = 0)]
    public class IntProperty : ScriptableObject
    {
        [SerializeField] private int _value;

        public event Action<int> OnChange; 
        public int Value
        {
            get => _value;
            set
            {
                var oldValue = _value;
                _value = value;
                
                if (oldValue != _value)
                {
                    OnChange?.Invoke(_value);
                }
            }
        }
    }
}