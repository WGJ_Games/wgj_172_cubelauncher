using System;
using UnityEngine;
using UnityEngine.Events;

namespace Code.Base
{
    [CreateAssetMenu(fileName = "Event", menuName = "GameEvent", order = 0)]
    public class GameEvent : ScriptableObject
    {
        public Action Callback;
        
        public UnityEvent Response;
        public void InvokeEvent()
        {
            Callback?.Invoke();
        }
    }
}