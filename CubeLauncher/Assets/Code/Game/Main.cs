using System;
using System.Collections.Generic;
using Code.Base;
using de.jacklog.cubeLauncher.Code.Game.Targets;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class Main : MonoBehaviour
    {
        [SerializeField] private List<BaseTarget> _targets;
        [SerializeField] private GameEvent _victoryEvent;
        [SerializeField] private IntProperty _shots;
        [SerializeField] private int _numOfShots;
        private bool _victory;

        private void Start()
        {
            _victory = false;
            _shots.Value = _numOfShots;
        }

        private void Update()
        {
            if (_victory)
            {
                return;
            }
            
            foreach (var target in _targets)
            {
                if (target != null && target.isActiveAndEnabled)
                {
                    return;
                }
            }

            _victory = true;
            _victoryEvent.InvokeEvent();
        }
    }
}