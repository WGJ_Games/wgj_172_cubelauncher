using System;
using System.Runtime.InteropServices;
using Cinemachine;
using Code.Base;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _cam;
        [SerializeField] private GameEvent _victoryEvent;

        [SerializeField] private Transform _victoryPoint;
        private bool _lock;
        public bool Locked => _lock;

        private void Start()
        {
            _victoryEvent.Callback += OnVictory;
            _lock = false;
        }

        private void OnDestroy()
        {
            _victoryEvent.Callback -= OnVictory;
        }

        private void OnVictory()
        {
           SetCamera(_victoryPoint);
           _lock = true;
        }

        public void SetCamera(Transform target)
        {
            if (_lock)
            {
                return;
            }
            _cam.Follow = target;
            _cam.LookAt = target;
        }
    }
}