using System;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game.Level
{
    public class IceComponent : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            
            if (other.gameObject.CompareTag("Projectile"))
            {
                Destroy(gameObject);
            }
        }
    }
}