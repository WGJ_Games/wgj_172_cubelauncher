using Code.Base;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class DelagateAnimationEvent : MonoBehaviour
    {
        [SerializeField] private GameEvent _event;

        public void TriggerEvent()
        {
            _event.InvokeEvent();
        }
    }
}