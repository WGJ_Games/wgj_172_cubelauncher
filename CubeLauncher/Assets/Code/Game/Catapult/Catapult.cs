using System;
using Cinemachine;
using Code.Base;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class Catapult : MonoBehaviour
    {
        [SerializeField] private Transform foot;
        [SerializeField] private Transform point;

        [SerializeField] private CatapultData _data;

        [SerializeField] private Projectile _prefab;
        [SerializeField] private Transform _spawnPoint;

        [SerializeField] private CameraController _camCtrl;

        [SerializeField] private GameEvent _resetEvent;
        [SerializeField] private GameEvent _defeatEvent;
        [SerializeField] private GameEvent _launchEvent;
        [SerializeField] private IntProperty _shots;
        private bool _lastShot;
        [SerializeField] private Animator _animator;

        private void Start()
        {
            _data.OnLaunch.AddListener(PrepareLaunch);
            _launchEvent.Callback += LaunchCube;
            _resetEvent.Callback += ResetCamera;
            _shots.OnChange += CheckShotsLeft;
            _lastShot = false;
            
        }

        private void CheckShotsLeft(int shots)
        {
            if (shots <= 0)
            {
                _lastShot = true;
            }
        }

        private void ResetCamera()
        {
            if (!_camCtrl.Locked && _lastShot)
            {
                //No More Shots!
                _defeatEvent.InvokeEvent();
            }
            else
            {
                _camCtrl.SetCamera(transform);
            }
        }

        private void OnDisable()
        {
            _data.OnLaunch.RemoveAllListeners();
            _shots.OnChange -= CheckShotsLeft;
            _resetEvent.Callback -= ResetCamera;
            _launchEvent.Callback -= LaunchCube;
        }

        private void PrepareLaunch()
        {
            _animator.Play("Launch");
        }
        
        private void LaunchCube()
        {
            var projectile = Instantiate(_prefab);
            projectile.transform.position = _spawnPoint.position;

            var direction = (point.position - foot.position).normalized;
            direction *= _data.CalcStrengh();
            projectile.GetComponent<Rigidbody>().AddForce(direction);
            _camCtrl.SetCamera(projectile.transform);
            _shots.Value--;

        }
    }
}