using UnityEngine;
using UnityEngine.Events;

namespace de.jacklog.cubeLauncher.Code.Game
{
    [CreateAssetMenu(fileName = "Catapult", menuName = "Game/Catapult", order = 0)]
    public class CatapultData : ScriptableObject
    {
        public float Strength;
        public float MinStrengh;
        public float MaxStrength;

        public UnityEvent OnLaunch;

        public float CalcStrengh()
        {
            return Mathf.Lerp(MinStrengh, MaxStrength, Strength);
        }
    }
}