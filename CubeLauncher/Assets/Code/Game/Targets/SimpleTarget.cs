using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game.Targets
{
    public class SimpleTarget : BaseTarget
    {
        [SerializeField] private float _radius;
        [SerializeField] private float _power;

        protected override void Explode()
        {
            Vector3 explosionPos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, _radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(_power, explosionPos, _radius, 3.0F);
            }
            
            Destroy(gameObject);
        }
    }
}