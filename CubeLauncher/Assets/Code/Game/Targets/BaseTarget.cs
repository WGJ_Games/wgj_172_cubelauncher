using System;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game.Targets
{
    public abstract class BaseTarget : MonoBehaviour
    {
        private float _wait;

        private void Start()
        {
            _wait = 1f;
        }

        private void Update()
        {
            if (_wait > 0)
            {
                _wait -= Time.deltaTime;
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            if (_wait > 0)
            {
                return;
            }
            if (other.gameObject.CompareTag("Wall")||other.gameObject.CompareTag("Projectile")||other.gameObject.CompareTag("Floor"))
            {
                Explode();
            }
        }

        protected abstract void Explode();
    }
}