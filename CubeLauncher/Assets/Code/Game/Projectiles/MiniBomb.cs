using System;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class MiniBomb : MonoBehaviour
    {
        [SerializeField] private float _radius;
        [SerializeField] private float _power;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Wall")||other.gameObject.CompareTag("Target")||other.gameObject.CompareTag("Floor"))
            {
                Explode();
            }
        }
        
        protected void Explode()
        {
            Vector3 explosionPos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, _radius);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(_power, explosionPos, _radius, 3.0F);
            }
            
            Destroy(gameObject);
        }
    }
}