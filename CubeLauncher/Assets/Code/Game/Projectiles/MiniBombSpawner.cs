using System;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Object = UnityEngine.Object;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class MiniBombSpawner : MonoBehaviour
    {
        private float _cooldown;
        [SerializeField] private float _reloadTime;
        [SerializeField] private MiniBomb _miniBomb;
        [SerializeField] private float _bombBoost;

        private void Start()
        {
            _cooldown = _reloadTime;
        }

        private void Update()
        {
            if (_cooldown > 0)
            {
                _cooldown -= Time.deltaTime;
            }
            else
            {
                _cooldown = _reloadTime;
                var bomb = Instantiate(_miniBomb, transform);
                bomb.GetComponent<Rigidbody>().AddForce(Vector3.down*_bombBoost);
            }
        }
    }
}