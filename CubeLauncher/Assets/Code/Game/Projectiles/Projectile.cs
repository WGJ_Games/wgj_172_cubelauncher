using System;
using Code.Base;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Game
{
    public class Projectile : MonoBehaviour
    {
        private bool _impact;
        private Rigidbody _rigid;
        [SerializeField] private GameEvent _resetEvent;
        private bool _reseted;
        private float _cooldown;

        private void Start()
        {
            _impact = false;
            _rigid = GetComponent<Rigidbody>();
           
        }

        private void OnEnable()
        {
            _cooldown = 1f;
        }

        private void Update()
        {
            if (_cooldown > 0)
            {
                _cooldown -= Time.deltaTime;
                return;
            }
            
            if (!_reseted)
            {
                CheckVelocity();
                CheckPosition();
            }
            
        }

        private void CheckPosition()
        {
            if (transform.position.y < -10)
            {
                Reset();
            }
        }

        private void CheckVelocity()
        {
            if (_rigid.velocity.magnitude < 0.025 && _impact == false)
            {
                Reset();
            }
        }

        private void Reset()
        {
            _reseted = true;
            _resetEvent.InvokeEvent();
            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            // if (other.gameObject.tag == "Wall" && !_impact)
            // {
            //     _impact = true;
            // }
        }
    }
}