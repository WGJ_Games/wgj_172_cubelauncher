using System;
using UnityEngine;

namespace de.jacklog.cubeLauncher.Code.Menu
{
    public class RotateMe : MonoBehaviour
    {
        [SerializeField] private float _rotationSpeed;

        private void Update()
        {
            transform.Rotate(new Vector3(0,_rotationSpeed,0));
        }
    }
}