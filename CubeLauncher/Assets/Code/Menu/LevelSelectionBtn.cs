using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace de.jacklog.cubeLauncher.Code.Menu
{
    public class LevelSelectionBtn : MonoBehaviour
    {
        [SerializeField] private string _levelName;
        private void Start()
        {
            var btn = GetComponent<Button>();
            btn.onClick.AddListener(() =>
            {
                SceneManager.LoadScene(_levelName);
            });
        }
    }
}